### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 80aa5a00-e052-11eb-2003-a172ad327da0
using Pkg

# ╔═╡ 3e867a9b-bf80-4ca3-a6fb-eadf57cbfd3e
Pkg.activate("Project.toml")

# ╔═╡ cf15afcf-30da-4b75-9b3c-6f9f87e162a4
using PlutoUI

# ╔═╡ 998a574e-d468-4c44-a2c6-bc17c6977639
md"# Supplementary Assignmement"

# ╔═╡ f73b6b9d-0743-46c5-a0ec-c7c4ade879a9
md"## Problem 2"

# ╔═╡ 5c1f89cb-7df7-493c-b92d-aaa64aebfeab
@enum NODomain one two three four

# ╔═╡ a429ac24-e324-412b-b3f8-dbfeaf551c7a
mutable struct CSP
name::String
value::Union{Nothing,NODomain}
forbidden_values::Vector{NODomain}
counter::Int64
end

# ╔═╡ 60b123bd-702d-4db2-aa9c-03c20c66d04f
struct NumCSP
digt::Vector{CSP}
constraints::Vector{Tuple{CSP,CSP}}
end

# ╔═╡ 3e42e898-c618-4cf3-9640-cf8752d718fc
count = rand(setdiff(Set([one,two,three,four]), Set([four,one])))

# ╔═╡ 8701b21b-8c76-4b7c-aa84-1ebb90651f11
function ConstraintSolver(cs::NumCSP, variables)
		for firstNo in cs.digt
			if firstNo.counter == 4
			return []
		else
			secondNo = rand(setdiff(Set([one,two,three,four]),
			Set(firstNo.forbidden_values)))
			
			firstNo.value = secondNo
			
		for firstConstraint in cs.constraints
			if !((firstConstraint[1] == firstNo) || (firstConstraint[2] ==
			firstNo))
			continue
		else
			if firstConstraint[1] == firstNo
			push!(firstConstraint[2].forbidden_values, secondNo)
			firstConstraint[2].counter += 1
			
		else
			push!(firstConstraint[1].forbidden_values, secondNo)
			firstConstraint[1].counter += 1
			

			end
		end
		end
		
		push!(variables, firstNo.name => secondNo)
		end
		end
		return variables
end					
					

# ╔═╡ 2dc292fb-891e-402a-9bf8-4229735f0572
begin
 x1 = CSP("X1",nothing, [], 0);
 x2 = CSP("X2", nothing, [], 0);
 x3 = CSP("X3", nothing, [], 0);
 x4 = CSP("X4", nothing, [], 0);
 x5 = CSP("X5", nothing, [], 0);
 x6 = CSP("X6", nothing, [], 0);
 x7 = CSP("X7", nothing, [], 0);
end

# ╔═╡ a8f3a97b-e97d-4110-b915-489d0f169e3b
execution = NumCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ Cell order:
# ╟─998a574e-d468-4c44-a2c6-bc17c6977639
# ╟─f73b6b9d-0743-46c5-a0ec-c7c4ade879a9
# ╠═80aa5a00-e052-11eb-2003-a172ad327da0
# ╠═3e867a9b-bf80-4ca3-a6fb-eadf57cbfd3e
# ╠═cf15afcf-30da-4b75-9b3c-6f9f87e162a4
# ╠═5c1f89cb-7df7-493c-b92d-aaa64aebfeab
# ╠═a429ac24-e324-412b-b3f8-dbfeaf551c7a
# ╠═60b123bd-702d-4db2-aa9c-03c20c66d04f
# ╠═3e42e898-c618-4cf3-9640-cf8752d718fc
# ╠═8701b21b-8c76-4b7c-aa84-1ebb90651f11
# ╠═2dc292fb-891e-402a-9bf8-4229735f0572
# ╠═a8f3a97b-e97d-4110-b915-489d0f169e3b
