### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ e948f821-6cf1-47e8-946d-d08f2288edf1
using Pkg

# ╔═╡ 21bf9005-a867-4dd1-8d78-5248ade0f25d
Pkg.activate("Project.toml")

# ╔═╡ f86ef3f9-8784-4d2c-91d1-43e6769899e6
Pkg.add("Evolutionary");using Evolutionary

# ╔═╡ 920e3db1-bf42-4346-be4b-886270e56a56
using PlutoUI

# ╔═╡ 3e7db972-fc00-40e7-8233-625ef80ff7d4
using DataFrames, Statistics, Plots

# ╔═╡ 4ded9fb0-df48-11eb-386f-8ff7897a5bd3
md"# Supplementary Assignmement"

# ╔═╡ 849b4108-c771-4d56-96bf-dedfec8f1d6a
md"## Problem 1"

# ╔═╡ c7d9833b-ebf2-44a7-bfac-d54e21ffb1fe
function fitness(fit)
    # we want the expression `a + 2b + 3c + 4d + 5e - 42`
    # to be as close to 0 as possible
    level = fit.firstclass[1] +
            2 * fit.firstclass[2] +
            3 * fit.firstclass[3] +
            4 * fit.firstclass[4] +
            5 * fit.firstclass[5]

    core(level - 42)
end


# ╔═╡ 43dd25cb-7b8c-489c-b805-58ffee5a4f89
begin
	
    firstclass::Array
    fitness

    balancedScale() = new(Array(Int, 5), nothing)
    balancedScale(firstclass) = new(firstclass, nothing)
#end

function create_entity(num)
    
    balancedScale(rand(Int, 5) % 43)
end
end

# ╔═╡ f65f0feb-8441-4b71-b61b-265939900032
function selection(pick)
    if pick[1].fitness == 0
        return
    end

   
    for i in 1:length(pick)
        produce([1, i])
    end
end


# ╔═╡ b1326c3c-5751-4582-8877-ed0261e539d5
function crossover(cross)
    child = EqualityMonster()

    
    num_parents = length(cross)
    for i in 1:length(cross[1].firstclass)
        parent = (rand(Uint) % num_parents) + 1
        child.firstclass[i] = cross[parent].firstclass[i]
    end

    child
end


# ╔═╡ 4a54838b-c2b3-4919-833d-c80b9a0cb78e
function mutate(fit)
    
    rand(Float64) < 0.8 && return

    rand_element = rand(Uint) % 5 + 1
    fit.firstclass[rand_element] = rand(Int) % 43
end


# ╔═╡ f5034eb6-9ed1-4bfa-bcb7-8aee8d1273eb


# ╔═╡ Cell order:
# ╟─4ded9fb0-df48-11eb-386f-8ff7897a5bd3
# ╠═849b4108-c771-4d56-96bf-dedfec8f1d6a
# ╠═e948f821-6cf1-47e8-946d-d08f2288edf1
# ╠═21bf9005-a867-4dd1-8d78-5248ade0f25d
# ╠═920e3db1-bf42-4346-be4b-886270e56a56
# ╠═3e7db972-fc00-40e7-8233-625ef80ff7d4
# ╠═f86ef3f9-8784-4d2c-91d1-43e6769899e6
# ╠═43dd25cb-7b8c-489c-b805-58ffee5a4f89
# ╠═c7d9833b-ebf2-44a7-bfac-d54e21ffb1fe
# ╠═f65f0feb-8441-4b71-b61b-265939900032
# ╠═b1326c3c-5751-4582-8877-ed0261e539d5
# ╠═4a54838b-c2b3-4919-833d-c80b9a0cb78e
# ╠═f5034eb6-9ed1-4bfa-bcb7-8aee8d1273eb
